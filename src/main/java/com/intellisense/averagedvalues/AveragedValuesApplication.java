package com.intellisense.averagedvalues;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AveragedValuesApplication {

    public static void main(String[] args) {
        SpringApplication.run(AveragedValuesApplication.class, args);
    }

}
