package com.intellisense.averagedvalues.interfaces.rest.average;

import com.intellisense.averagedvalues.application.ports.in.AverageCalculator;
import com.intellisense.averagedvalues.domain.model.enums.PeriodTypeEnum;
import com.intellisense.averagedvalues.interfaces.rest.average.dtos.AverageRequest;
import com.intellisense.averagedvalues.interfaces.rest.average.dtos.AverageResponse;
import com.intellisense.averagedvalues.interfaces.rest.average.mapper.AverageResponseMapper;
import com.intellisense.averagedvalues.interfaces.rest.average.validation.AverageRequestValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Slf4j
@Validated
@Controller
@RequestMapping(value = AveragedValuesRestController.URI_ROOT,
        produces = AveragedValuesRestController.CONTENT_TYPE_RESPONSE)
@RequiredArgsConstructor
public class AveragedValuesRestController implements AveragedValuesRest {

    public static final String LOCATION = "Location";
    public static final String URI_ROOT = "/api/v1";
    public static final String AVERAGE_PATH = "/average";

    public static final String CONTENT_TYPE_RESPONSE = "application/json;charset=UTF-8";

    private final AverageCalculator averageCalculator;
    private final AverageRequestValidator averageRequestValidator;

    @PostMapping(AVERAGE_PATH)
    @Override
    public ResponseEntity<AverageResponse> calculateAverage(@Valid @RequestBody final AverageRequest averageRequest) {
        try {
            log.info("Rest Calculate Average init: {}", averageRequest);

            final var averageCalculation = averageCalculator.calculateAverage(PeriodTypeEnum.valueOfPeriod(averageRequest.getPeriod()));
            if (log.isDebugEnabled()) {
                log.debug("Rest Calculate Average response:" + averageCalculation);
            }
            final var averageResponse = AverageResponseMapper.INSTANCE.mapToAverageResponse(averageCalculation);
            return ResponseEntity.status(HttpStatus.OK).header(LOCATION, URI_ROOT + AVERAGE_PATH)
                    .body(averageResponse);
        } finally {
            log.debug("Rest Calculate Average finish");
        }

    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(averageRequestValidator);
    }
}
