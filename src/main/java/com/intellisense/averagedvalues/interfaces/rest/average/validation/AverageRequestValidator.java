package com.intellisense.averagedvalues.interfaces.rest.average.validation;

import com.intellisense.averagedvalues.domain.model.enums.PeriodTypeEnum;
import com.intellisense.averagedvalues.interfaces.rest.average.dtos.AverageRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
@RequiredArgsConstructor
public class AverageRequestValidator implements org.springframework.validation.Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return AverageRequest.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final var averageRequest = (AverageRequest) target;
        validateAmountsOnShippingCost(errors, averageRequest);
    }

    private void validateAmountsOnShippingCost(Errors errors, AverageRequest averageRequest) {

        if (!PeriodTypeEnum.isPresent(averageRequest.getPeriod())) {
            errors.rejectValue(
                    "period",
                    "invalidPeriod",
                    "Period {1} value. Period should be valid.");
        }
    }
}
