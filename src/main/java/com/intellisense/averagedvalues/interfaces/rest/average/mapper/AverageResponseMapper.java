package com.intellisense.averagedvalues.interfaces.rest.average.mapper;

import com.intellisense.averagedvalues.domain.model.AverageCalculation;
import com.intellisense.averagedvalues.interfaces.rest.average.dtos.AverageResponse;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface AverageResponseMapper {

    AverageResponseMapper INSTANCE = Mappers.getMapper(AverageResponseMapper.class);

    AverageResponse mapToAverageResponse(AverageCalculation averageCalculation);
}
