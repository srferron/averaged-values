package com.intellisense.averagedvalues.interfaces.rest.average.dtos;

import lombok.Value;

import java.util.List;

@Value
public class AverageResponse {

    private List<AssetResponse> assets;
}
