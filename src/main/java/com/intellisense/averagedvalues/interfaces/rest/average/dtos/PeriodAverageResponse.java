package com.intellisense.averagedvalues.interfaces.rest.average.dtos;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class PeriodAverageResponse {
    private String periodId;
    private BigDecimal average;
}
