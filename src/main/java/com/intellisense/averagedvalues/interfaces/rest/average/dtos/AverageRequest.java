package com.intellisense.averagedvalues.interfaces.rest.average.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
public class AverageRequest {

    @JsonProperty("period")
    private final Integer period;
}
