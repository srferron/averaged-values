package com.intellisense.averagedvalues.interfaces.rest.average.dtos;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class MetricAverageResponse {
    private String metricId;
    private List<PeriodAverageResponse> periodsAverage;
}
