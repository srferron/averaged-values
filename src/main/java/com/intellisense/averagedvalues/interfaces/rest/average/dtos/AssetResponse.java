package com.intellisense.averagedvalues.interfaces.rest.average.dtos;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class AssetResponse {
    private String assetId;
    private List<MetricAverageResponse> metrics;
}
