package com.intellisense.averagedvalues.interfaces.rest.average;


import com.intellisense.averagedvalues.interfaces.rest.average.dtos.AverageRequest;
import com.intellisense.averagedvalues.interfaces.rest.average.dtos.AverageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;

public interface AveragedValuesRest {

    @Operation(summary = "Calculate average of a given period")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Calculated",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = AverageResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid period", content = @Content)})
    ResponseEntity<AverageResponse> calculateAverage(@Valid AverageRequest averageRequest);

}
