package com.intellisense.averagedvalues.application.ports.out;

import com.intellisense.averagedvalues.domain.model.vos.MeasuresVO;

public interface Measurements {

    MeasuresVO obtainMeasures();
}
