package com.intellisense.averagedvalues.application.ports.in;

import com.intellisense.averagedvalues.domain.model.AverageCalculation;
import com.intellisense.averagedvalues.domain.model.enums.PeriodTypeEnum;

public interface AverageCalculator {

    AverageCalculation calculateAverage(PeriodTypeEnum period);
}
