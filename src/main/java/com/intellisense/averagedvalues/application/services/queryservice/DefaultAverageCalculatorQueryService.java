package com.intellisense.averagedvalues.application.services.queryservice;

import com.intellisense.averagedvalues.application.ports.in.AverageCalculator;
import com.intellisense.averagedvalues.application.ports.out.Measurements;
import com.intellisense.averagedvalues.domain.model.AverageCalculation;
import com.intellisense.averagedvalues.domain.model.enums.PeriodTypeEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DefaultAverageCalculatorQueryService implements AverageCalculator {

    final Measurements measurements;

    @Override
    public AverageCalculation calculateAverage(PeriodTypeEnum period) {
        final var measures = measurements.obtainMeasures();

        return AverageCalculation.makeAverageCalculationFromPeriod(period, measures);
    }
}
