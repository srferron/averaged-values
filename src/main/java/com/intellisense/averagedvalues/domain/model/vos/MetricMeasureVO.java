package com.intellisense.averagedvalues.domain.model.vos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;

@Value
@Builder
@AllArgsConstructor
@Jacksonized
public class MetricMeasureVO {
    private BigDecimal value;
}
