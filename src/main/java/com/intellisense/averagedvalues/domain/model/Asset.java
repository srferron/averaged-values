package com.intellisense.averagedvalues.domain.model;

import com.intellisense.averagedvalues.domain.model.enums.PeriodTypeEnum;
import com.intellisense.averagedvalues.domain.model.vos.AssetVO;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Builder
public class Asset {
    private String assetId;
    private List<MetricAverage> metrics;

    static Asset makeAssetFromPeriod(PeriodTypeEnum period, AssetVO asset) {
        return Asset.builder()
                .assetId(asset.getAssetId())
                .metrics(asset.getMetrics().stream()
                        .map(metric -> {
                            final var ranges = asset.obtainRanges(period);
                            final var periodRangesAverageMap = metric.calculatePeriodRangesAverage(ranges);
                            final var periodRangesAverage = mapPeriodsAverage(periodRangesAverageMap);

                            return MetricAverage.makeMetricAverage(metric.getMetricId(), periodRangesAverage);
                        }).collect(Collectors.toList()))
                .build();
    }

    private static List<PeriodAverage> mapPeriodsAverage(Map<String, BigDecimal> periodRangesAverageMap) {
        return periodRangesAverageMap
                .entrySet().stream()
                .map(periodAverageEntry ->
                        PeriodAverage.makePeriodAverage(periodAverageEntry.getKey(), periodAverageEntry.getValue()))
                .collect(Collectors.toList());
    }
}
