package com.intellisense.averagedvalues.domain.model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class PeriodAverage {
    private String periodId;
    private BigDecimal average;

    static PeriodAverage makePeriodAverage(String periodId, BigDecimal average) {
        return PeriodAverage.builder()
                .periodId(periodId)
                .average(average)
                .build();
    }
}
