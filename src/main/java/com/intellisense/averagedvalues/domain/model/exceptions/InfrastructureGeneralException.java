package com.intellisense.averagedvalues.domain.model.exceptions;

public class InfrastructureGeneralException extends RuntimeException {

    public InfrastructureGeneralException(String message) {
        super(message);
    }
}
