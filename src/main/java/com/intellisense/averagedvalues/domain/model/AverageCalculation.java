package com.intellisense.averagedvalues.domain.model;

import com.intellisense.averagedvalues.domain.model.enums.PeriodTypeEnum;
import com.intellisense.averagedvalues.domain.model.vos.MeasuresVO;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class AverageCalculation {

    private List<Asset> assets;

    public static AverageCalculation makeAverageCalculationFromPeriod(PeriodTypeEnum period, MeasuresVO measures) {

        return AverageCalculation.builder()
                .assets(buildAssets(period, measures)
                ).build();
    }

    private static List<Asset> buildAssets(PeriodTypeEnum period, MeasuresVO measures) {
        return measures.getAssets().stream()
                .map(asset -> Asset.makeAssetFromPeriod(period, asset))
                .collect(Collectors.toList());
    }
}
