package com.intellisense.averagedvalues.domain.model.vos;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Value
@Builder
@Jacksonized
public class MetricVO {

    private String metricId;
    private List<MetricMeasureVO> metricMeasures;

    public Map<String, BigDecimal> calculatePeriodRangesAverage(List<PeriodRangeVO> periodsRanges) {
        return periodsRanges.stream()
                .map(periodRange -> {
                    final var rangeEntryIndexesSize = new AtomicInteger(periodRange.getMetricMeasureIndexes().size());
                    final var totalMetricMeasures = aggregateMetricMeasures(periodRange, rangeEntryIndexesSize);

                    if (currentPeriodDoesNotHaveValidMeasures(rangeEntryIndexesSize, totalMetricMeasures)) {
                        return Map.entry(periodRange.getPeriodId(), totalMetricMeasures);
                    }
                    return Map.entry(periodRange.getPeriodId(),
                            totalMetricMeasures.divide(BigDecimal.valueOf(rangeEntryIndexesSize.get()), 2, RoundingMode.HALF_UP));

                })
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    private BigDecimal aggregateMetricMeasures(PeriodRangeVO periodRange, AtomicInteger rangeEntryIndexesSize) {
        return periodRange.getMetricMeasureIndexes().stream()
                .map(timePeriodIndex -> {
                    final var metricMeasure = this.getMetricMeasures().get(timePeriodIndex);
                    if (Objects.isNull(metricMeasure.getValue())) {
                        rangeEntryIndexesSize.getAndDecrement();
                    }
                    return metricMeasure.getValue();
                })
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private static boolean currentPeriodDoesNotHaveValidMeasures(AtomicInteger rangeEntryIndexesSize, BigDecimal totalMetricMeasures) {
        return totalMetricMeasures.equals(BigDecimal.ZERO) || rangeEntryIndexesSize.get() == 0;
    }
}
