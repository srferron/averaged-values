package com.intellisense.averagedvalues.domain.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@AllArgsConstructor
public enum PeriodTypeEnum {

    TEN_MINUTES(10), THIRTY_MINUTES(30), SIXTY_MINUTES(60);

    private static final Map<String, PeriodTypeEnum> LOOKUP = new HashMap<>(PeriodTypeEnum.values().length);

    private final Integer periodType;

    public static boolean isPresent(Integer period) {
        return Stream.of(PeriodTypeEnum.values())
                .map(PeriodTypeEnum::getPeriodType)
                .collect(Collectors.toSet())
                .contains(period);
    }

    public static PeriodTypeEnum valueOfPeriod(Integer period) {
        for (PeriodTypeEnum p : values()) {
            if (p.periodType.equals(period)) {
                return p;
            }
        }
        return null;
    }
}
