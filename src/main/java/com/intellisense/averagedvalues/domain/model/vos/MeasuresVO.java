package com.intellisense.averagedvalues.domain.model.vos;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Value
@Builder
@Jacksonized
public class MeasuresVO {

    private List<AssetVO> assets;
}
