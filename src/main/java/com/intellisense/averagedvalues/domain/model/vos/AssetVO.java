package com.intellisense.averagedvalues.domain.model.vos;

import com.intellisense.averagedvalues.domain.model.enums.PeriodTypeEnum;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Value
@Builder
@Jacksonized
public class AssetVO {
    private String assetId;
    private List<MetricVO> metrics;
    private List<Instant> timePoints;

    public List<PeriodRangeVO> obtainRanges(PeriodTypeEnum period) {
        final var initialTimePoint = this.timePoints.get(0);
        final var finalTimePoint = this.timePoints.get(timePoints.size() - 1);
        final var between = ChronoUnit.MINUTES.between(initialTimePoint, finalTimePoint) + 1;
        final var numberOfRanges = BigDecimal.valueOf(between)
                .divide(BigDecimal.valueOf(period.getPeriodType()), 0, RoundingMode.UP)
                .intValue();

        final var periods = obtainPeriods(period, initialTimePoint, numberOfRanges);

        return IntStream.range(0, periods.size() - 1)
                .mapToObj(periodsIndex -> {
                    final var key = String.format("%s - %s",
                            periods.get(periodsIndex).truncatedTo(ChronoUnit.MINUTES),
                            periods.get(periodsIndex + 1).truncatedTo(ChronoUnit.MINUTES));

                    return PeriodRangeVO.builder()
                            .periodId(key)
                            .metricMeasureIndexes(obtainMetricPointsForAnIndexRange(periods, periodsIndex))
                            .build();
                })
                .collect(Collectors.toList());
    }

    private List<Integer> obtainMetricPointsForAnIndexRange(List<Instant> periods, int periodsIndex) {
        return IntStream.range(0, this.timePoints.size())
                .filter(timePointsIndex ->
                        !this.timePoints.get(timePointsIndex).isBefore(periods.get(periodsIndex))
                                && this.timePoints.get(timePointsIndex).isBefore(periods.get(periodsIndex + 1)))
                .mapToObj(timePointsIndex -> timePointsIndex)
                .collect(Collectors.toList());
    }

    private List<Instant> obtainPeriods(PeriodTypeEnum period, Instant initialTimePoint, int numberOfRanges) {
        return IntStream.range(0, numberOfRanges + 1)
                .mapToObj(rangeIndex -> {
                    final var minutesToAdd = period.getPeriodType() * rangeIndex;
                    return initialTimePoint.plus(Duration.ofMinutes(minutesToAdd));
                })
                .collect(Collectors.toList());
    }
}
