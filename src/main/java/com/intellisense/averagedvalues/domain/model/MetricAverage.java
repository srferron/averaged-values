package com.intellisense.averagedvalues.domain.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class MetricAverage {
    private String metricId;
    private List<PeriodAverage> periodsAverage;

    static MetricAverage makeMetricAverage(String metricId, List<PeriodAverage> periodsAverage) {
        return MetricAverage.builder()
                .metricId(metricId)
                .periodsAverage(periodsAverage)
                .build();
    }
}
