package com.intellisense.averagedvalues.domain.model.vos;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class PeriodRangeVO {
    private String periodId;
    private List<Integer> metricMeasureIndexes;

}
