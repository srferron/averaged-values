package com.intellisense.averagedvalues.infrastructure.files.adapters;

import com.intellisense.averagedvalues.application.ports.out.Measurements;
import com.intellisense.averagedvalues.domain.model.vos.MeasuresVO;
import com.intellisense.averagedvalues.infrastructure.files.DatasetReader;
import com.intellisense.averagedvalues.infrastructure.files.mapper.DataSetMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SeriesAdapter implements Measurements {

    final DatasetReader dataSetReader;

    @Override
    public MeasuresVO obtainMeasures() {
        final var dataSet = dataSetReader.readDataSet();

        return DataSetMapper.INSTANCE.mapDataSet(dataSet);
    }
}
