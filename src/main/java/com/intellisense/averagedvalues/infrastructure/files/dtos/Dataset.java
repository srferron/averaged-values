package com.intellisense.averagedvalues.infrastructure.files.dtos;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class Dataset {
    private final Map<String, Asset> assets = new HashMap<>();

    @JsonAnySetter
    public void assignAssets(String name, Asset value) {
        assets.put(name, value);
    }

}
