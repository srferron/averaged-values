package com.intellisense.averagedvalues.infrastructure.files.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;

@Slf4j
@Configuration
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DatasetReaderProperties {

    @Value("${intellisense.properties.scheme}")
    @Getter
    private String scheme;

    @Value("${intellisense.properties.host}")
    @Getter
    private String host;

    @Value("${intellisense.properties.path}")
    @Getter
    private String path;

    @PostConstruct
    public final void init() {
        if (log.isDebugEnabled()) {
            log.debug("DatasetReader Service: {}", this);
        }
        Assert.hasLength(scheme, "intellisense.properties.scheme is required");
        Assert.hasLength(host, "intellisense.properties.host is required");
        Assert.hasLength(path, "intellisense.properties.path is required");
    }

}
