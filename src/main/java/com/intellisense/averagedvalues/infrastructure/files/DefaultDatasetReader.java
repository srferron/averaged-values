package com.intellisense.averagedvalues.infrastructure.files;

import com.intellisense.averagedvalues.domain.model.exceptions.InfrastructureGeneralException;
import com.intellisense.averagedvalues.infrastructure.files.configuration.DatasetReaderProperties;
import com.intellisense.averagedvalues.infrastructure.files.dtos.Dataset;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultDatasetReader implements DatasetReader {

    private final RestOperations restTemplate;
    private final DatasetReaderProperties datasetReaderProperties;

    @Override
    public Dataset readDataSet() {
        var urlTemplate = "";
        urlTemplate = UriComponentsBuilder
                .newInstance().scheme(datasetReaderProperties.getScheme()).host(datasetReaderProperties.getHost()).path(datasetReaderProperties.getPath()).build()
                .toUriString();
        try {
            HttpEntity<Dataset> requestEntity = new HttpEntity<>(null, new HttpHeaders());

            final var datasetResponseEntity =
                    restTemplate.exchange(urlTemplate, HttpMethod.GET, requestEntity, Dataset.class);

            if (log.isDebugEnabled()) {
                log.debug("Dataset readed: {}", datasetResponseEntity.getBody());
            }
            return datasetResponseEntity.getBody();
        } catch (HttpClientErrorException | HttpServerErrorException httpErrorException) {
            log.error("Error reading file. Exception : {}", httpErrorException.getMessage());
            throw new InfrastructureGeneralException(httpErrorException.getMessage());
        }

    }

}
