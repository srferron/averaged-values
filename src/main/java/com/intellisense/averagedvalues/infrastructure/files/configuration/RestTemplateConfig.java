package com.intellisense.averagedvalues.infrastructure.files.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.stream.IntStream;

@Configuration
@RequiredArgsConstructor
public class RestTemplateConfig {

    private final ObjectMapper objectMapper;

    @Bean
    public RestTemplate restTemplate() {
        final RestTemplate restTemplate = new RestTemplate();

        replaceConverter(restTemplate, createMappingJacksonHttpMessageConverter(), MappingJackson2HttpMessageConverter.class);

        return restTemplate;
    }

    private void replaceConverter(RestTemplate restTemplate, HttpMessageConverter converter, Class converterClass) {
        IntStream.range(0, restTemplate.getMessageConverters().size())
                .filter(i -> converterClass.isInstance(restTemplate.getMessageConverters().get(i)))
                .forEach(i -> restTemplate.getMessageConverters().set(i, converter));
    }

    private MappingJackson2HttpMessageConverter createMappingJacksonHttpMessageConverter() {
        final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(objectMapper);

        return converter;
    }
}
