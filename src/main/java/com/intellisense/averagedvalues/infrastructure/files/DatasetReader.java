package com.intellisense.averagedvalues.infrastructure.files;

import com.intellisense.averagedvalues.infrastructure.files.dtos.Dataset;

public interface DatasetReader {

    Dataset readDataSet();
}
