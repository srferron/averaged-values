package com.intellisense.averagedvalues.infrastructure.files.mapper;

import com.intellisense.averagedvalues.domain.model.vos.AssetVO;
import com.intellisense.averagedvalues.domain.model.vos.MeasuresVO;
import com.intellisense.averagedvalues.domain.model.vos.MetricMeasureVO;
import com.intellisense.averagedvalues.domain.model.vos.MetricVO;
import com.intellisense.averagedvalues.infrastructure.files.dtos.Asset;
import com.intellisense.averagedvalues.infrastructure.files.dtos.Dataset;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface DataSetMapper {

    DataSetMapper INSTANCE = Mappers.getMapper(DataSetMapper.class);

    @Mapping(source = "assets", target = "assets",
            qualifiedByName = "mapAssets")
    MeasuresVO mapDataSet(Dataset request);

    @Named("mapAssets")
    default List<AssetVO> mapAssets(Map<String, Asset> request) {
        if (Objects.isNull(request)) {
            return null;
        }
        return request.entrySet().stream()
                .map(entry -> AssetVO.builder()
                        .assetId(entry.getKey())
                        .metrics(mapMetrics(entry))
                        .timePoints(mapTimePoints(entry))
                        .build())
                .collect(Collectors.toList());
    }

    private List<Instant> mapTimePoints(Map.Entry<String, Asset> entry) {
        return entry.getValue().obtainTimePoints().stream().map(Instant::parse).collect(Collectors.toList());
    }

    private List<MetricVO> mapMetrics(Map.Entry<String, Asset> entry) {
        return entry.getValue().obtainMetrics().entrySet().stream()
                .map(metricEntry ->
                        MetricVO.builder()
                                .metricId(metricEntry.getKey())
                                .metricMeasures(metricEntry.getValue().stream()
                                        .map(Asset::castMetric)
                                        .map(MetricMeasureVO::new)
                                        .collect(Collectors.toList()))
                                .build()
                )
                .collect(Collectors.toList());
    }
}
