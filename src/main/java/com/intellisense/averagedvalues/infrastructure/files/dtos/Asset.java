package com.intellisense.averagedvalues.infrastructure.files.dtos;

import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Asset {
    private final String TIME_KEY = "time";
    private final Map<String, Object> entries = new HashMap<>();

    @JsonAnySetter
    public void assignEntries(String name, Object value) {
        entries.put(name, value);
    }

    public Map<String, List<Object>> obtainMetrics() {
        return entries.entrySet().stream()
                .filter(entry -> !TIME_KEY.equals(entry.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> (List<Object>) entry.getValue()));
    }

    public List<String> obtainTimePoints() {
        return entries.entrySet().stream()
                .filter(entry -> TIME_KEY.equals(entry.getKey()))
                .flatMap(entry -> ((List<String>) entry.getValue()).stream()).collect(Collectors.toList());
    }

    public static BigDecimal castMetric(Object object) {
        if (object instanceof Double) {
            return BigDecimal.valueOf((Double) object);
        }

        if (object instanceof Integer) {
            return BigDecimal.valueOf((Integer) object);
        }

        return null;
    }

}
