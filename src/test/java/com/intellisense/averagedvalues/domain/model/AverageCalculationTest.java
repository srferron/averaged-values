package com.intellisense.averagedvalues.domain.model;

import com.intellisense.averagedvalues.domain.model.enums.PeriodTypeEnum;
import com.intellisense.averagedvalues.domain.model.vos.AssetVO;
import com.intellisense.averagedvalues.domain.model.vos.MeasuresVO;
import com.intellisense.averagedvalues.domain.model.vos.MetricMeasureVO;
import com.intellisense.averagedvalues.domain.model.vos.MetricVO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class AverageCalculationTest {

    @Test
    @DisplayName("Calculation casuistry")
    void shouldValidateAverageCalculationCasuistry() {
        // Given
        final var measuresMock = obtainMeasuresMock();

        // When
        final var average = AverageCalculation.makeAverageCalculationFromPeriod(PeriodTypeEnum.TEN_MINUTES, measuresMock);

        // Then
        final var periods = average.getAssets().get(0).getMetrics().get(0).getPeriodsAverage();
        assertThat(periods.get(0).getAverage()).isEqualByComparingTo(BigDecimal.valueOf(25.50));
        assertThat(periods.get(1).getAverage()).isEqualByComparingTo(BigDecimal.valueOf(21.00));
        assertThat(periods.get(2).getAverage()).isEqualByComparingTo(BigDecimal.valueOf(38.75));
        assertThat(periods.get(3).getAverage()).isEqualByComparingTo(BigDecimal.valueOf(0));
        assertThat(periods.get(4).getAverage()).isEqualByComparingTo(BigDecimal.valueOf(25.50));
        assertThat(periods.get(5).getAverage()).isEqualByComparingTo(BigDecimal.valueOf(25.50));
        assertThat(periods.get(6).getAverage()).isEqualByComparingTo(BigDecimal.valueOf(25.00));
        assertThat(periods.get(7).getAverage()).isEqualByComparingTo(BigDecimal.valueOf(20.00));
        assertThat(periods.get(8).getAverage()).isEqualByComparingTo(BigDecimal.valueOf(32.00));
        assertThat(periods.get(9).getAverage()).isEqualByComparingTo(BigDecimal.valueOf(25.00));
        assertThat(periods.get(10).getAverage()).isEqualByComparingTo(BigDecimal.valueOf(0));

    }

    private MeasuresVO obtainMeasuresMock() {
        final var now = ZonedDateTime.parse("2022-02-03T10:00:00+01:00[Europe/Madrid]").toInstant();
        return MeasuresVO.builder()
                .assets(List.of(
                        AssetVO.builder()
                                .assetId("100")
                                .timePoints(
                                        IntStream.range(1, 22)
                                                .mapToObj(index -> now.plus(Duration.ofMinutes(5 * index)))
                                                .collect(Collectors.toList())
                                )
                                .metrics(List.of(
                                        MetricVO.builder()
                                                .metricId("1001")
                                                .metricMeasures(
                                                        getMetricMeasureVOS(Arrays.asList(BigDecimal.valueOf(21L), BigDecimal.valueOf(30L),
                                                                BigDecimal.valueOf(21L), null,
                                                                BigDecimal.valueOf(33.5), BigDecimal.valueOf(44L),
                                                                null, null,
                                                                BigDecimal.valueOf(21L), BigDecimal.valueOf(30L),
                                                                BigDecimal.valueOf(21L), BigDecimal.valueOf(30L),
                                                                BigDecimal.valueOf(20L), BigDecimal.valueOf(30L),
                                                                BigDecimal.valueOf(20L), null,
                                                                BigDecimal.valueOf(29L), BigDecimal.valueOf(35L),
                                                                BigDecimal.valueOf(20L), BigDecimal.valueOf(30L),
                                                                null))
                                                )
                                                .build()))
                                .build()
                ))
                .build();
    }

    private List<MetricMeasureVO> getMetricMeasureVOS(List<BigDecimal> values) {
        return values.stream().map(MetricMeasureVO::new).collect(Collectors.toList());
    }
}
