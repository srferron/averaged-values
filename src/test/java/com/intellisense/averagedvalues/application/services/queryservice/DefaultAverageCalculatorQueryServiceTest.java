package com.intellisense.averagedvalues.application.services.queryservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.intellisense.averagedvalues.application.ports.out.Measurements;
import com.intellisense.averagedvalues.domain.model.enums.PeriodTypeEnum;
import com.intellisense.averagedvalues.domain.model.vos.MeasuresVO;
import com.intellisense.averagedvalues.util.ReadFiles;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class DefaultAverageCalculatorQueryServiceTest {

    private ReadFiles readFiles = new ReadFiles();

    @InjectMocks
    private DefaultAverageCalculatorQueryService averageCalculator;

    @Spy
    private Measurements measurements;

    @Test
    void givenMultipleAssets_whenHappyPath_thenCalculatesTheAverage() throws JsonProcessingException, JSONException {
        // Given
        final var measuresMock = (MeasuresVO) readFiles.getJsonFileAsObject("json/measures.json", MeasuresVO.class);
        final var averageCalculationExpected = readFiles.getFileAsString("json/average_calculation.json");

        // When
        when(measurements.obtainMeasures()).thenReturn(measuresMock);
        final var average = averageCalculator.calculateAverage(PeriodTypeEnum.TEN_MINUTES);

        // Then
        JSONAssert.assertEquals(readFiles.writeValueAsString(average), averageCalculationExpected,
                JSONCompareMode.STRICT);

    }

}
