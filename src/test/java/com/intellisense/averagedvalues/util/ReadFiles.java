package com.intellisense.averagedvalues.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
@RequiredArgsConstructor
@Getter
public class ReadFiles {

    private final ObjectMapper objectMapper;
    private static final Path BASE_TEST_RESOURCES = Paths.get("src", "test", "resources");

    public ReadFiles() {
        final var om = new ObjectMapper();
        om.registerModule(new JavaTimeModule());
        om.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        objectMapper = om;
    }

    public String writeValueAsString(Object value) throws JsonProcessingException {
        return objectMapper.writeValueAsString(value);
    }

    public String getFileAsString(String path) {
        String fileString = null;
        try {
            fileString = Files.readString(BASE_TEST_RESOURCES.resolve(Paths.get(path)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileString;
    }

    public Object getJsonFileAsObject(String path, Class clazz) throws JsonProcessingException {
        final String fileContent = getFileAsString(path);
        return objectMapper.readValue(fileContent, clazz);
    }
}
