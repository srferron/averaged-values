package com.intellisense.averagedvalues.archunit;

import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.domain.JavaModifier;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tngtech.archunit.base.DescribedPredicate.not;
import static com.tngtech.archunit.core.domain.JavaClass.Predicates.resideInAnyPackage;
import static com.tngtech.archunit.core.domain.JavaClass.Predicates.simpleNameEndingWith;
import static org.assertj.core.api.Assertions.assertThat;

@AnalyzeClasses(packages = "com.intellisense.averagedvalues",
        importOptions = ImportOption.DoNotIncludeTests.class)
class HexagonalArchitectureRulesTest {

    final String ROOT_PACKAGE = "com.intellisense.averagedvalues";

    final JavaClasses importedClasses =
            new ClassFileImporter().withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
                    .importPackages(ROOT_PACKAGE);

    @Test
    void applicationToInfrastructure() {
        final ArchRule applicationToInfrastructure = ArchRuleDefinition.noClasses().that()
                .resideInAPackage(ROOT_PACKAGE + ".application..").should()
                .dependOnClassesThat().resideInAPackage(ROOT_PACKAGE + ".infrastructure..");

        applicationToInfrastructure.check(importedClasses);
    }

    @Test
    void domainToInfrastructure() {
        final ArchRule domainToInfrastructure = ArchRuleDefinition.noClasses().that()
                .resideInAPackage(ROOT_PACKAGE + ".application..").should()
                .dependOnClassesThat().resideInAPackage(ROOT_PACKAGE + ".infrastructure..");

        domainToInfrastructure.check(importedClasses);
    }

    @Test
    void domainToInterfaces() {
        final ArchRule domainToInterfaces = ArchRuleDefinition.noClasses().that()
                .resideInAPackage(ROOT_PACKAGE + ".application..").should()
                .dependOnClassesThat().resideInAPackage(ROOT_PACKAGE + ".infrastructure..");

        domainToInterfaces.check(importedClasses);
    }

    @Test
    void domainToApplication() {
        final ArchRule domainToApplication = ArchRuleDefinition.noClasses().that()
                .resideInAPackage(ROOT_PACKAGE + ".application..").should()
                .dependOnClassesThat().resideInAPackage(ROOT_PACKAGE + ".infrastructure..");

        domainToApplication.check(importedClasses);
    }

    @Test
    void infrastructureToInterfaces() {
        final ArchRule infrastructureToInterfaces = ArchRuleDefinition.noClasses().that()
                .resideInAPackage(ROOT_PACKAGE + ".application..").should()
                .dependOnClassesThat().resideInAPackage(ROOT_PACKAGE + ".infrastructure..");

        infrastructureToInterfaces.check(importedClasses);
    }

    @Test
    void interfacesToInfrastructure() {
        final ArchRule interfacesToInfrastructure = ArchRuleDefinition.noClasses().that()
                .resideInAPackage(ROOT_PACKAGE + ".application..").should()
                .dependOnClassesThat().resideInAPackage(ROOT_PACKAGE + ".infrastructure..");

        interfacesToInfrastructure.check(importedClasses);
    }

    @Test
    void restToRepository() {
        final ArchRule restToRepository = ArchRuleDefinition.noClasses().that()
                .resideInAPackage(ROOT_PACKAGE + ".application..").should()
                .dependOnClassesThat().resideInAPackage(ROOT_PACKAGE + ".infrastructure..");

        restToRepository.check(importedClasses);
    }

    @Test
    void repositoryToRest() {
        final ArchRule repositoryToRest = ArchRuleDefinition.noClasses().that()
                .resideInAPackage(ROOT_PACKAGE + ".application..").should()
                .dependOnClassesThat().resideInAPackage(ROOT_PACKAGE + ".infrastructure..");

        repositoryToRest.check(importedClasses);
    }

    @Test
    void VOsAndDTOsImmutable() {
        final JavaClasses importedClasses =
                new ClassFileImporter().importPackages(ROOT_PACKAGE)
                        .that(resideInAnyPackage(new String[]{"..vos..", "..dtos.."})
                                .and(not(simpleNameEndingWith("Builder"))));

        assertClassImmutable(importedClasses);
    }

    @Test
    void applicationToInterfaces() {
        final ArchRule applicationToInterfaces = ArchRuleDefinition.noClasses().that()
                .resideInAPackage(ROOT_PACKAGE + ".application..").should()
                .dependOnClassesThat().resideInAPackage(ROOT_PACKAGE + ".interfaces..");

        applicationToInterfaces.check(importedClasses);
    }

    private void assertClassImmutable(JavaClasses importedClasses) {
        importedClasses.stream().forEach(javaClass -> {
            final List<String> allowedMethodNames = obtainAllowedMethodNames(javaClass);
            javaClass.getMethods().stream().forEach(javaMethod -> {
                if (javaMethod.getName().startsWith("set")
                        && javaMethod.getModifiers().contains(JavaModifier.PUBLIC)) {
                    assertThat(allowedMethodNames)
                            .as("Public method %s is not allowed begin with setter", javaClass.getName())
                            .contains(javaMethod.getName());
                }

            });

            javaClass.getFields().stream().forEach(javaField -> {
                assertThat(javaField.getModifiers())
                        .as("Fields on class %s should be 'private'", javaClass.getName())
                        .contains(JavaModifier.PRIVATE);
                assertThat(javaField.getModifiers())
                        .as("Fields on class %s should be 'final'", javaClass.getName())
                        .contains(JavaModifier.FINAL);

            });
        });
    }

    private List<String> obtainAllowedMethodNames(JavaClass javaClass) {
        final List<String> defaultAllowedMethodNames =
                List.of("toString", "equals", "hashCode", "builder");
        final List<String> gettersAndSetters = obtainGettersAndSetters(javaClass);
        return Stream.concat(defaultAllowedMethodNames.stream(), gettersAndSetters.stream())
                .collect(Collectors.toList());
    }

    private List<String> obtainGettersAndSetters(JavaClass javaClass) {
        final List<String> gettersAndSetters = new ArrayList<>();
        javaClass.getFields().forEach(f -> {
            if (javaClass.tryGetMethod("get" + StringUtils.capitalize(f.getName())).isPresent()) {
                gettersAndSetters.add("get" + StringUtils.capitalize(f.getName()));
            }
            if (javaClass.tryGetMethod("is" + StringUtils.capitalize(f.getName())).isPresent()) {
                gettersAndSetters.add("is" + StringUtils.capitalize(f.getName()));
            }
            if (javaClass.tryGetMethod("set" + StringUtils.capitalize(f.getName())).isPresent()) {
                gettersAndSetters.add("set" + StringUtils.capitalize(f.getName()));
            }
        });
        return gettersAndSetters;
    }

}
