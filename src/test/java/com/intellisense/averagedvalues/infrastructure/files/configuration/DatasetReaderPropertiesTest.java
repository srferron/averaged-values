package com.intellisense.averagedvalues.infrastructure.files.configuration;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@Import(DatasetReaderProperties.class)
@TestPropertySource(properties = {
        "intellisense.properties.scheme=thescheme",
        "intellisense.properties.host=thehost",
        "intellisense.properties.path=/theuri"})
class DatasetReaderPropertiesTest {

    @Autowired
    private DatasetReaderProperties datasetReaderProperties;

    @Test
    void testConfig() {
        assertThat(datasetReaderProperties.getScheme()).isEqualTo("thescheme");
        assertThat(datasetReaderProperties.getHost()).isEqualTo("thehost");
        assertThat(datasetReaderProperties.getPath()).isEqualTo("/theuri");
    }
}
