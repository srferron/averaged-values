package com.intellisense.averagedvalues.infrastructure.files.configuration;

import com.intellisense.averagedvalues.domain.model.exceptions.InfrastructureGeneralException;
import com.intellisense.averagedvalues.infrastructure.files.DefaultDatasetReader;
import com.intellisense.averagedvalues.infrastructure.files.dtos.Dataset;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DatasetReaderClientTest {

    @Mock
    private RestTemplate restTemplate;

    private DefaultDatasetReader datasetReader;

    private final String scheme = "http";
    private final String host = "localhost:8081";
    private final String path = "/test.dataprovider";

    @BeforeEach
    public void init() {
        final var datasetReaderProperties =
                new DatasetReaderProperties(scheme, host, path);

        datasetReader = new DefaultDatasetReader(restTemplate, datasetReaderProperties);
    }

    @Test
    void givenDatasetReaderCall_whenDatasetEndpointProduceAnHTTPException_thenItIsMappedToAModelException() {
        // Given
        final var httpClientErrorException =
                new HttpClientErrorException(HttpStatus.NOT_FOUND);

        final var urlTemplate = UriComponentsBuilder
                .newInstance().scheme(scheme).host(host).path(path).build()
                .toUriString();

        // When
        when(restTemplate.exchange(urlTemplate, HttpMethod.GET, new HttpEntity<>(null, new HttpHeaders()),
                Dataset.class)).thenThrow(httpClientErrorException);

        // Then
        Assertions.assertThrows(InfrastructureGeneralException.class, () -> {
            datasetReader.readDataSet();
        });
    }

}

