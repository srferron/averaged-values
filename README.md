# averaged-values

## Summary

Microservice to process dataset to get the average between time brackets measures.

It has been developed following a Hexagonal archetype, verified with ArchUnit tests. The justification for this is
because, even if it´s not so big at the moment, it´s that maybe could grow a lot and require a high level of changes on
future. That´s why it´s expected to require a big level of adaptability (High level of cohesion and decoupling) and the
reason why I opted for this architecture. Obviously this test has over-engineering doubt to it´s to hire with
Intellisense.

## Run

    $ gradlew clean build

	$ docker build -t averaged-values .

    $ docker run --name averaged-values -d -p 8080:8080 averaged-values

## Related

* [Gitlab config repo](https://gitlab.com/srferron/averaged-values)

## Technologies

* Java 11
* Spring Boot 2.6.3
* Docker

## Troubleshooting

Enhancements:

* I required more time to check some behaviours like return null instead of cero when there is only null values on a period.
* Security for REST endpoints
* Internationalization of the application (Messages from files, possible currency changes, etc...).
* Improve resilience with retries and circuit breaking.
* Audit (Maybe use aspectJ to store all request and response on flows) and observability enhancements.
* Ensure contracts between APIs. There are two different options:
    1. Agreeing contracts between the server and client sync sides through API First (Open API). Generate Controllers
       from the contracts.
    2. With contract tests, sharing stubs and jars with the DTOs
* Create integration test with WireMocks

	
	

	
